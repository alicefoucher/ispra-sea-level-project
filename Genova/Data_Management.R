# Environment
rm(list=ls())

# Library
library(dplyr)
library(tidyr)
library(stringr)
library('TideHarmonics')
library(imputeTS)
library(circular)

###### Data ######
# Source : http://dati.isprambiente.it
# https://mareografico.it

## Extract the Data
data <- data.frame()

for (i in 0:9) {
  data1 <- read.table(paste("Data/Genova/Livello_Ultrasuoni_200", i,".txt", sep = ""), header = FALSE, sep=";",
                      col.names = c("date","hour","level"))
  data2 <- read.table(paste("Data/Genova/Pressione_Atmosferica_200", i,".txt", sep = ""), header = FALSE, sep=";",
                      col.names = c("date","hour","PAtm"))
  data3 <- read.table(paste("Data/Genova/Temperatura_Aria_200", i,".txt", sep = ""), header = FALSE, sep=";",
                      col.names = c("date","hour","TA"))
  #data4 <- read.table(paste("Data/Genova/Temperatura_Acqua_200", i,".txt", sep = ""), header = FALSE, sep=";",
  #col.names = c("date","hour","TAcqua"))
  data5 <- read.table(paste("Data/Genova/Velocita_Vento_200", i,".txt", sep = ""), header = FALSE, sep=";",
                      col.names = c("date","hour","VV"))
  data6 <- read.table(paste("Data/Genova/Direzione_Vento_200", i,".txt", sep = ""), header = FALSE, sep=";",
                      col.names = c("date","hour","DV"))
  
  data1 <- data1 %>%
    left_join(data2) %>%
    left_join(data3) %>%
    #left_join(data4) %>%
    left_join(data5) %>%
    left_join(data6) 
  
  data <- data %>% 
    bind_rows(data1)
}

data1 <- read.csv("Data/Genova/Genova.Liv1.csv",header = FALSE, sep = ";", col.names = c("wrong","date","hour","level"))
data2 <- read.csv("Data/Genova/Genova.PAtm.csv",header = FALSE, sep = ";", col.names = c("wrong","date","hour","PAtm"))
#data3 <- read.csv("Data/Genova/Genova.RH.csv",header = FALSE, sep = ";", col.names = c("wrong","date","hour","RH"))
data4 <- read.csv("Data/Genova/Genova.TA.csv",header = FALSE, sep = ";", col.names = c("wrong","date","hour","TA"))
#data5 <- read.csv("Data/Genova/Genova.TAcqua.csv",header = FALSE, sep = ";", col.names = c("wrong","date","hour","TAcqua"))
data6 <- read.csv("Data/Genova/Genova.DV.csv",header = FALSE, sep = ";", col.names = c("wrong","date","hour","VV","DV"))

data1 <- data1 %>%
  left_join(data2) %>%
  #left_join(data3) %>%
  left_join(data4) %>%
  #left_join(data5) %>%
  left_join(data6) 

rm(data2,data3,data4,data5,data6)


# Homogenize the data
data <- data %>%
  mutate(level=level/100) %>% #level in meters
  slice(-1:-5, -length(data$level))
data1 <- data1 %>% 
  dplyr::select(-wrong) %>%
  slice(-1) 
data1$level <- as.numeric(data1$level)
data1$PAtm <- as.numeric(data1$PAtm)
data1$TA <- as.numeric(data1$TA)
#data1$TAcqua <- as.numeric(data1$TAcqua)
data1$VV <- as.numeric(data1$VV)
data1$DV <- as.numeric(data1$DV)

data$date <- as.character(as.Date(data$date, format = "%d/%m/%Y"))
data1$hour <- str_replace(data1$hour,fixed("."),":")


# Concatenate
data <- data %>%
  bind_rows(data1)


## Keep the data every hour 
# To have more stable values for the level, we average, for each hour, all its taken values.
data <- data %>%
  mutate(gpe = substr(hour,1,2))
temp <- data %>%
  group_by(date, gpe) %>%
  mutate(level = replace_na(level, mean(level, na.rm = TRUE))) %>%
  summarise_at(vars(level), list(level = mean))
data <- data %>%
  filter(substr(hour,4,5)=="00")
data <- temp %>%
  inner_join(data, by = c("date", "gpe")) %>%
  dplyr::select(-gpe, -level.y) %>% rename(level=level.x)


## Right DateTime format
data$date <- paste(data$date,data$hour,sep=" ")
data$date <- strptime(data$date,format='%Y-%m-%d %H:%M', tz = "UTC")
data <- data %>% 
  dplyr::select(-hour) %>%
  rename(DateTime = date)
data$DateTime <- as.POSIXct(data$DateTime)

## Write Data ##
#write.csv(data, file = "Data/Genova/Genova_sea_level.csv")

###### Desc Stat ###### 
summary(data)
plot(data$DateTime, data$level,  type = "l", 
     xlab = "Time", ylab = "Level") 

###### Imputation: response variable (level) ######

data <- read.csv("Data/Genova/Genova_sea_level.csv") %>%
  dplyr::select(-X)
data$DateTime <- as.POSIXct(data$DateTime, tz="UTC")

# Distribution of the missing values 
ggplot_na_distribution(data$level, data$DateTime)

## Imputation of the response variable: sea levels:
# by Exponential Weighted Moving Average
data <- data %>%
  mutate(level = na_ma(level, k = 12, weighting = "exponential")) # 24 hours time window

###### Tide Harmonics ######

# Fit tidal data using any of over 60 harmonic constituents.

hfit1 <- ftide(x = data$level, dto = data$DateTime, hcn = hc60)
summary(hfit1)

# Write our new database with our residual series
data2 <- data %>%
  dplyr::select(-level) %>%
  mutate(levelr = hfit1$residuals)

###### Missing values about circular data #####

# Missing values for wind direction linked to values for speed wind
# -> 0: error measurements 
temp <- data2 %>% filter(is.na(DV)==TRUE) %>%
  filter(!is.na(VV)==TRUE)
summary(temp)
summary(data2)
filter(data2, is.na(data2$DV)==TRUE)$VV
data2[is.na(data2$DV)==TRUE,5] <- NA

# Imputation by simple moving average (circular mean)
data2$DV <- circular(data2$DV, units = "degrees")
for(i in 1:length(data2$DV)) {
  if(is.na(data2$DV[i])==TRUE){
    #data2$VV[i] = NA
    data2$DV[i] = mean.circular(data2$DV[i-12:i+12], na.rm=TRUE)+360 #positive
  }
}
data2$DV <- as.numeric(data2$DV)


###### Imputation of the covariates: ####

# Imputation of missing values by Exponential Weighted Moving Average
data2 <- data2 %>%
  mutate(PAtm = na_ma(PAtm, k = 12, weighting = "exponential"), # window of a year: 24*365.25/2-1
         TA = na_ma(TA, k = 12, weighting = "exponential"),
         #TAcqua = na_ma(TAcqua, k = 12, weighting = "exponential"),
         VV = na_ma(VV, k = 12, weighting = "exponential"),
         #DV = na_ma(DV, k = 12, weighting = "exponential")
  )

#write.csv(data2, file = "Data/Genova/Genova_sea_levelr.csv")

# data2<-read.csv("Data/Genova/Genova_sea_levelr.csv")

###### Impute extreme values: #####
data3 <- data2
data3$levelr[data3$levelr > 0.3999283 ] <- NA
data3$levelr[data3$levelr < -0.3999283 ] <- NA
ggplot_na_distribution(data3$levelr)
data3 <- data3 %>%
  mutate(levelr = na_ma(levelr, k = 12, weighting = "exponential"))

#write.csv(data3, file = "Data/Genova/Genova_sea_levelr_extr.csv")
