library(ggplot2)
ggplot(data = data.frame(x = model$residuals), aes(x = x)) +
  geom_histogram(bins = 400, fill = "blue", alpha = 0.5) +  # Histogramme
  geom_density(color = "red") +  # Densité
  labs(title = "Histogram of ARIMA Residuals")
acf(d1)
Box.test(model$residuals, lag = 10, type = "Ljung-Box")
standardized_residuals <- (model$residuals-mean(model$residuals))/sd(model$residuals)
plot(model$fitted, sqrt(standardized_residuals), xlab = "Fitted values", ylab = "Square root of the standardised residuals", main = "Residual Dispersion")
abline(lm(standardized_residuals~model$fitted),col="red")
library(stats)
result<-Box.test(model$residuals^2, lag = 10, type = "Ljung-Box")
result$p.value
update.packages("lmtest")
arch.test(x = model$residuals, lags = 10)
